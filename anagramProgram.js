//3. logic for anagram program with its time complexity. (for large strings)
let str1 = 'Listen'
let str2 = 'neha'

lowerCaseStr1 = str1.toLowerCase()
lowerCaseStr2 = str2.toLowerCase()

word1 = lowerCaseStr1.split('').sort().join('')
word2 = lowerCaseStr2.split('').sort().join('')

if(word1 === word2){
    console.log(`Given string its anagram`);
}else{
    console.log(`Given string its not a anagram`)
}

// -------------------------------------
//using function 
// function anagram(str1, str2){
//  const cleanstr1 = str1.toLowerCase()
//   console.log(cleanstr1, 'toLowercase');
//  word1 = cleanstr1.split('').sort().join('')
// console.log(word1, 'after sorting')
//  const  cleanstr2 =  str2.toLowerCase()
//     word2 = cleanstr2.split('').sort().join('')
//    console.log(word2, 'after sorting');
//   console.log(cleanstr2, 'toLowercase');
//   if (word1 === word2){
//    console.log('given string is anagram');
//   }else{
//     console.log('not anagram');
//   }

// }

// const wordOne = 'LISENT'
// const wordTwo = 'tnesil'
// anagram(wordOne,wordTwo)