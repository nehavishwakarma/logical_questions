// 10. Code to find even numbers in a list

// for (let i = 1; i <= 30; i++){
//     if (i % 2 == 0){
//         console.log(i);
//     }
// }

// for (let i = 0; i <= 30; i++){
//     if (i % 2 !== 0){
//         console.log(`print odd number ${i}`);
//     }
// }

let array = [2,4,6,8];
let missingArray = [];
let min = Math.min(...array); //1
let max = Math.max(...array); //8
// console.log(max)

for (let i=min; i< max; i++){
    if(array.indexOf(i)<0){ // indexOf  return its position , not there in -1
        console.log(array.indexOf(i));
        missingArray.push(i)
    }
}
console.log("missingArray", ...missingArray); //missingArray 3 5 7
 

