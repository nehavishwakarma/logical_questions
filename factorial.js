// factorial number 
// 5! = 5*4*3*2*1
const factorial = 5
let temp = 1
for(let i = factorial; i>=1; i--){
   temp *= i
}
console.log(temp);

//with recursion (function call itself)
// function fact(n){
//     if ( n ==0 || n==1) {
//         return 1
//     }    
//   return n * fact(n-1)
// }
// let data = fact(5)
// console.log(data);