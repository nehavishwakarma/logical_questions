// // 7. WAP to print Fibonacci series with recursion
//    //example = 0,1,1,2,3,5,8,13,21,34
//    //0+1=1,1+1=2,2+2=4,4+3=7,7+4=11,11+5=16.....etc
//    let febbo = 5
//    var n1 = 0,  n2 = 1, n3 = 0
//    for(let i = 1; i<febbo; i++){
//     if(i == 1){
//         console.log('0/n1');
//         console.log('1/n2');
//     }
//     n3 = n1+n2
//     n1 = n2
//     n2 = n3
//     console.log(n3);
//    }
//    ----------------------------------------------------
//with recursion

function febo(n) {
        if(n === 1){
            return 0;
        }
        if( n === 2){
            return 1; 
        }
       return febo(n-1) + febo(n-2)
    }
    
let value = febo(5)

console.log(value);
