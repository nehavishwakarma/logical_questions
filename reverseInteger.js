// 2. WAP to reverse an integer without converting it to a string, without using any builtin methods. 

let number = 2384575
console.log("Original value", number);
let reverse = 0
while(number != 0){
    reverse = (reverse*10) + (number%10)
    number = ~~(number/10) 
}
console.log(`reverse integer without converting it to a string ${reverse}` );