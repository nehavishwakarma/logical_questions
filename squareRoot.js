//12. WAP for getting a square root of a given number
let number = 16
let value = number**0.5
console.log(value);
// ----------------------------------------


// let number = 81
// let guess = number/2
// let precision = 0.000001
// while(Math.abs(guess * guess - number)>precision){
//     guess =  (guess + number/ guess)/2
// }
// console.log(~~guess);

// -------------------------------------------
// using function 
// function squareRoot(number) {
//   let guess = number / 2;
//   const precision = 0.01; // Desired level of precision

//   while (Math.abs(guess * guess - number) > precision) {
//     guess = (guess + number / guess) / 2; // Update guess using the Newton's method formula
//   }
//   return ~~guess;
// }
// const number = 81;
// const squareRootValue = squareRoot(number);
// console.log((squareRootValue));