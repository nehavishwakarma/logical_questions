//2. Swap Values without using any variable a=10, b=12

a = 10
b = 12
console.log(`original value of a ${a}`);
console.log(`original value of b ${b}`);
a = a + b
b = a - b
a = a - b
console.log(`after swap value of a ${a}`);
console.log(`after swap value of b ${b}`);
